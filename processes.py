"""modulo que amplifica o reduce el sonido dado por una muestra"""

import array
import sys

import config


def ampli(sound, factor: float):

    for nsample in range(len(sound)):
        if sound[nsample] * factor>= config.max_amp:
            sound[nsample] = config.max_amp

        elif sound[nsample] * factor<= -config.max_amp:
            sound[nsample] = -config.max_amp

        elif sound[nsample] * factor < config.max_amp and sound[nsample] * factor > -config.max_amp:
            sound[nsample] = int(sound[nsample] * factor)

    return sound


def reduce(sound, factor:int):

    nsamples = config.samples_second * int(sys.argv[4])
    sound = array.array('h' , [0] * nsamples)

    if factor>0:
        del sound[::factor]

        return sound
    else:
        print('ERROR')


def extend(sound, factor: int):

    nsamples = config.samples_second * int(sys.argv[4])

    n=0

    if factor > 0:
        for i in range(factor, nsamples , fator):
            if i > factor:
                i *= n
                sound.insert(i, (sound[i - 1]) + sound[i] // 2)
            else:
                sound.insert(i, (sound[i-1] + sound[i] // 2))

        return sound
    else:
        print('ERROR: tiene que ser mayor a 0')
