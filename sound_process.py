import sys

import sources
import sinks
import processes

def parse_args():

    if len(sys.argv) != 8:
        print(f'Usage: {sys.argv[0]} <source> <source_argv> <process> <process_args> <sink> <sink_args> <duration>')
        exit(0)

    source = sys.argv[1]
    source_args = sys.argv[2]
    process = sys.argv[3]
    process_args = sys.argv[4]
    sink = sys.argv[5]
    sink_args = sys.argv[6]
    duration= float(sys.argv[7])
    return source, source_args, process, process_args, sink, sink_args, duration

def main():
    source, source_args, process, process_args, sink, sink_args,dur= parse_args()

    if source == 'sin':
        sound= sources.sin(duration=dur, freq=int(source_args))
    elif source == 'constant':
        sound= sources.constant(duration=dur, positive=bool(source_args))
    elif source == 'square':
        sound= sources.square(duration=dur, freq= int(source_args))
    else:
        sys.exit("Unkonwn source")


    if process == 'ampli':
        processes.ampli(sound=sound, factor=float(process_args))
    elif process == 'reduce':
        processes.reduce(sound=sound, factor=int(process_args))
    elif process == 'extend':
        processes.extend(sound=sound, factor=int(process_args))
    else:
        sys.exit("Unknown process")


    if sink == "play":
        sinks.play(sound)
    elif sink == "draw":
        sinks.draw(sound=sound, period= float(sink_args), source= sound)
    elif sink == "store":
        sinks.store(sound=sound, path=str(sink_args))
    else:
        sys.exit("Unknown sink")

if __name__ == '__main__':
    main()