import sys

import sources
import sinks


def parse_args():
    if len(sys.argv) != 4:
        print(f"Usage: {sys.argv[0]} <source> <sink> <duration>")
        exit(0)
    source = sys.argv[1]
    sink = sys.argv[2]

    duration = float(sys.argv[3])
    return source, sink, duration


def main():
    source, sink, dur = parse_args()

    if source == 'sin':
        sound = sources.sin(duration=dur, freq=400)
    elif source == 'constant':
        sound = sources.constant(duration=dur, positive=True)
    elif source == 'square':
        sound = sources.square(duration=dur, freq=400)
    else:
        sys.exit("Unknown source")

    if sink == "play":
        sinks.play(sound)
    if sink == "draw":
        sinks.draw(sound=sound, period=0.0001, source=source)
    if sink == "store":
        sinks.store(sound=sound, path= "stored.wav")
    else:
        sys.exit("Unknown sink")


if __name__ == '__main__':
    main()
