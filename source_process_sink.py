import sys

import config
import sources
import sinks
import process

def parse_args():
    if len(sys.argv) != 5:
        print(f"Usage: {sys.argv[0]} <source> <process> <sink> <duration>")
        exit(0)
    source = sys.argv[1]
    proces = sys.argv[2]
    sink = sys.argv[3]

    duration = float(sys.argv[4])
    return source, proces, sink, duration


def main():
    source, proces, sink, dur = parse_args()

    if source == 'sin':
        sound = sources.sin(duration=dur, freq=400)
    elif source == 'constant':
        sound = sources.constant(duration=dur, positive=True)
    elif source == 'square':
        sound = sources.square(duration=dur, freq=400)
    else:
        sys.exit("Unknown source")

    if proces == 'ampli':
        process.ampli(sound=sound,factor= 20)
    elif proces == 'reduce':
        process.reduce(sound=sound, factor= 20)
    elif proces == 'extend':
        process.extend(sound=sound, factor= 20)
    else:
        sys.exit("Unknown process")

    if sink == "play":
        sinks.play(sound)
    if sink == "draw":
        sinks.draw(sound=sound, period=0.0001, source=source)
    if sink == "store":
        sinks.store(sound=sound, path= "stored.wav")
    else:
        sys.exit("Unknown sink")


if __name__ == '__main__':
    main()
